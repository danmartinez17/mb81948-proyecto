const mongoose = require('mongoose');
const MONGO_HOSTNAME = '127.0.0.1';
const MONGO_PORT = '27017';
const MONGO_DB = 'mexwind';
const url = `mongoose.connect('mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}'`;
mongoose.connect(url, {useNewUrlParser: true});

// module.exports = {
//   url: "`mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`"
// };