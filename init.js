//Dependencias

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;
var cors = require('cors');
app.use(cors());

app.use(express.urlencoded({ extended: true }));
//Variables
//app.use(express.static(__dirname + '/build/default'));
app.use(express.static(__dirname)); // esto debe de cambiar para cuando se cree el build de polymer

//Iniciar el servidor
app.listen(port);
console.log('Server started on: http://localhost:' + port);

//Método GET
app.get('/', function (req, res) {
  //res.send('Recibido GET HTTP method');
  res.sendFile("index.html", {
    root: '.'
  });
});

//Método POST
app.post('/', function (req, res) {
  res.send('Recibido POST HTTP method');
});

//Método PUT
app.put('/', function (req, res) {
  res.send('Recibido PUT HTTP method');
});

//Método DELETE
app.delete('/', function (req, res) {
  res.send('Recibido DELETE HTTP method');
});