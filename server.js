const express     = require("express");
const bodyParser  = require("body-parser");
const cors        = require("cors");

const app         = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));
//app.use(express.static(__dirname + '/build/default'));
app.use(express.static(__dirname)); // esto debe de cambiar para cuando se cree el build de polymer


// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");
db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to the database!");
  })
  .catch(err => {
    console.log("Cannot connect to the database!", err);
    process.exit();
  });

  

// simple route
app.get("/inicio", (req, res) => {
  //res.send('Recibido GET HTTP method');
  res.sendFile("index.html", {
    root: '.'
  });
  //res.json({ message: "Welcome to bezkoder application." });
});


app.get("/login", (req, res) => {
  //res.send('Recibido GET HTTP method');
  res.sendFile("login.html", {
    root: '.'
  });
  //res.json({ message: "Welcome to bezkoder application." });
});
app.get("/ficha-cliente/:id", (req, res) => {
  //res.send('Recibido GET HTTP method');
  res.sendFile("ficha-cliente.html", {
    root: '.'
  });
  //res.json({ message: "Welcome to bezkoder application." });
});

require("./app/routes/client.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log('Mongoose version:', db.mongoose.version);
  console.log(`http://localhost:${PORT}`);
  console.log();
});
