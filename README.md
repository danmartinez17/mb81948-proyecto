# Proyecto Practitioner 2Ed México

Alumno: Daniel Martínez Cruz

## Descripción del proyecto

El proyecto consiste en un sistema de gestión de clientes, que nos permite saber cuántos clientes hay, cuantos debemos de atender, así como información de nuestros clientes.

## Features

- Uso de base de datos Mongo (API REST)
- Inicio de sesión
- Tablero de gestión de clientes
- Ficha de cliente
- Alta de nuevos usuarios y clientes

## To Do

El login requiere la correcta conexión con la base de datos, por ahora el login sólo manda a la página inicial

La información de la base de datos no se muestra al cargar la página, se utiliza un botón para hacer la carga de las mismas

Sólo está conectada una de las colecciones de la base de datos, se debe de completar la conexión con las demás bases

Se deben filtrar los cliente que atiende un usuario en base a  algún parámetro, por ahora el filtro está en los clientes cuyo estado sea Chiapas, ver 

```
exports.findAllPublished
```

 en el archivo `client.controller.js`

## Dependencias

```
@polymer/iron-ajax@3.0.1
bcrypt@5.0.0
body-parser@1.19.0
bulma@0.9.0
cors@2.8.5
express@4.17.1
mongoose@5.9.20
```

## Iniciar el proyecto

#### Importar bases de datos de muestra

Se generó una base de datos, puedes visualizar el cómo se creó desde el archivo de Excel `mexwind.xlsx` Para importar las bases de datos de muestra, se ejecuta desde la carpeta de database el archivo   `mongo-import.sh`, el cual importará todos los CSV

#### Lista de CSV

1. customers
2. orders
3. products
4. regions
5. states

Una vez importadas todos los CSV, tendremos 5 colecciones.

#### Instalamos las dependencias:

`npm install`

#### Ejecutamos el proyecto

`node server.js`