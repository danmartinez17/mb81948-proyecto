module.exports = app => {
  const misclientes = require("../controllers/client.controller.js");

  var clientes = require("express").Router();

  // Create a new Client
  clientes.post("/", misclientes.create);

  // Retrieve all Clients
  clientes.get("/", misclientes.findAll);

  // Retrieve all published Clients
  clientes.get("/published", misclientes.findAllPublished);

  // Retrieve a single Client with id
  clientes.get("/:id", misclientes.findOne);

  // Update a Client with id
  clientes.put("/:id", misclientes.update);

  // Delete a Client with id
  clientes.delete("/:id", misclientes.delete);

  // Create a new Client
  clientes.delete("/", misclientes.deleteAll);

  app.use("/api/clientes", clientes);
};
