const MONGO_USERNAME = 'user';
const MONGO_PASSWORD = 'passwword';
const MONGO_HOSTNAME = 'localhost';
const MONGO_PORT = '27017';
const MONGO_DB = 'mexwind';
module.exports = {
  url: `mongodb://${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}`
};
