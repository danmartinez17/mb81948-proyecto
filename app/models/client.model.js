module.exports = mongoose => {
  var schema = mongoose.Schema(
    {
    x: Number,
    CustomerID: String,
    Name: String,
    LastName: String,
    Genre: String,
    Age: Number,
    Address: String,
    State: String,
    State2: String,
    PostalCode: String,
    Country: String,
    Email: String,
    Employee: String,
    Birthdate: Date,
    Phone: String
    },
    { timestamps: true }
  );

  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Client = mongoose.model("customers", schema);
  return Client;
};
