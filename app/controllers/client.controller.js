const db = require("../models");
const Clientes = db.clientes;

// Crear y guardar un nuevo cliente
exports.create = (req, res) => {
  // Se valida la request, con el parámetro de CustomerID
  if (!req.body.CustomerID) {
    res.status(400).send({
      message: "El cliente no tiene todos los datos"
    });
    return;
  }

  // Create a Client
  const newCliente = new Clientes({
    x: req.body.x,
    CustomerID: req.body.CustomerID,
    Name: req.body.Name,
    LastName: req.body.LastName,
    Genre: req.body.Genre,
    Age: req.body.Age,
    Address: req.body.Address,
    State: req.body.State,
    State2: req.body.State2,
    PostalCode: req.body.PostalCode,
    Country: req.body.Country,
    Email: req.body.Email,
    Employee: req.body.Employee,
    Birthdate: req.body.Birthdate,
    Phone: req.body.Phone
  });

  // Save Client in the database
  newCliente
    .save(newCliente)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Client."
      });
    });
};

// Retrieve all Clients from the database.
exports.findAll = (req, res) => {
  const title = req.query.title;
  var condition = title ? {
    title: {
      $regex: new RegExp(title),
      $options: "i"
    }
  } : {};

  Clientes.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving clients."
      });
    });
};


// Find all limited
exports.findAllPublished = (req, res) => {
  Clientes.find({
    State: "Chiapas"
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving clients."
      });
    });
};

// Find a single Client with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Clientes.findById(id)
    .then(data => {
      if (!data)
        res.status(404).send({
          message: "Not found Client with id " + id
        });
      else res.send(data);
    })
    .catch(err => {
      res
        .status(500)
        .send({
          message: "Error retrieving Client with id=" + id
        });
    });
};

// Update a Client by the id in the request
exports.update = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Clientes.findByIdAndUpdate(id, req.body, {
      useFindAndModify: false
    })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Client with id=${id}. Maybe Client was not found!`
        });
      } else res.send({
        message: "Client was updated successfully."
      });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Client with id=" + id
      });
    });
};

// Delete a Client with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Clientes.findByIdAndRemove(id, {
      useFindAndModify: false
    })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Client with id=${id}. Maybe Client was not found!`
        });
      } else {
        res.send({
          message: "Client was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Client with id=" + id
      });
    });
};

// Delete all Clients from the database.
exports.deleteAll = (req, res) => {
  Clientes.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Clients were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while removing all clients."
      });
    });
};

// Find all published Clients
// exports.findAllPublished = (req, res) => {
//   Clientes.find({
//       published: true
//     })
//     .then(data => {
//       res.send(data);
//     })
//     .catch(err => {
//       res.status(500).send({
//         message: err.message || "Some error occurred while retrieving clients."
//       });
//     });
// };